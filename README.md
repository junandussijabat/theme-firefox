# theme-firefox

Theme for Firefox browser.

How to install :


1. Clone the folder or download the **"theme-firefox"** folder in the form of **zip/tar/tar.gz/tar.bz2** .
2. Extract the downloaded folder.
3. Open the **Mozilla Firefox browser**. type in the search field: 
> "**about: debugging**" (without quote).
4. Select the **"This Firefox"** option then select **"Load Temporary Add-on ..."**.
5. After the Windows window appears navigate to the location of the extracted folder, then select 
**"manifest.json"**.
6. Done.

or also you can install directly by visiting the following mozilla firefox add-ons link:
1. Elegant Black Hitman https://semawur.com/UCbyR
2. Elegant Black Hitman 2 https://semawur.com/x6dx1cAizP7
3. Elegant Black Hitman 3 https://semawur.com/2a6TzOHlONEB
